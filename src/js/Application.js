import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY);
  }
  setEmojis(emojis) {
    this.emojis = emojis;
    this.addBananas();
    const div = document.querySelector('#emojis');

    while (div.firstChild) {
      div.removeChild(div.firstChild);
    }

    this.emojis.forEach(element => {
      const p = document.createElement('p');
      p.innerText = element;
      div.appendChild(p);
    });
  }

  addBananas() {
    this.emojis = this.emojis.map(x => x + this.banana)
    // console.log(this.emojis)
  }
}
